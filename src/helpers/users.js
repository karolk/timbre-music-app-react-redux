import user1 from '../images/users/user-1.png';
import user3 from '../images/users/user-3.png';
import user2 from '../images/users/user-2.png';
import user4 from '../images/users/user-4.png';
import user5 from '../images/users/user-5.png';
import user6 from '../images/users/user-6.png';
import user7 from '../images/users/user-7.png';

const Users = [
	{
		photo: user1,
		name: 'Jay Mendez',
		title: 'Get Rich',
		artist: 'Tyga'
	},
	{
		photo: user4,
		name: 'Nina Hardy',
		title: 'Hookah',
		artist: 'Tyga'
	},
	{
		photo: user3,
		name: 'Dora Banks',
		title: 'I Fall apart',
		artist: 'Post Malone'
	},
	{
		photo: user7,
		name: 'Leonard Russell',
		title: 'We Dem Boyz',
		artist: 'Wiz Khalifa'
	},
	{
		photo: user6,
		name: 'Jeremy Massey',
		title: 'Boyah',
		artist: 'Showtek'
	},
	{
		photo: user2,
		name: 'Nelle Curry',
		title: 'Disciples',
		artist: 'On My Mind'
	},
	{
		photo: user5,
		name: 'Erik Evans',
		title: 'Brooks',
		artist: 'Hold It Down'
	}
];

export default Users;
