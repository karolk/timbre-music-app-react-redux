const songBaseDetails = {
	id: 362795841,
	readable: true,
	title: 'More Than You Know',
	title_short: 'More Than You Know',
	title_version: '',
	link: 'https://www.deezer.com/track/362795841',
	duration: '203',
	rank: '973224',
	explicit_lyrics: false,
	preview:
		'https://cdns-preview-b.dzcdn.net/stream/c-b8d9d0f77571c19a780ef6271db532e2-5.mp3',
	artist: {
		id: '7099473',
		name: 'Axwell / Ingrosso',
		link: 'https://www.deezer.com/artist/7099473',
		picture: 'https://api.deezer.com/artist/7099473/image',
		picture_small:
			'https://e-cdns-images.dzcdn.net/images/artist/f062b593c5913988424eb53344da5f0d/56x56-000000-80-0-0.jpg',
		picture_medium:
			'https://e-cdns-images.dzcdn.net/images/artist/f062b593c5913988424eb53344da5f0d/250x250-000000-80-0-0.jpg',
		picture_big:
			'https://e-cdns-images.dzcdn.net/images/artist/f062b593c5913988424eb53344da5f0d/500x500-000000-80-0-0.jpg',
		picture_xl:
			'https://e-cdns-images.dzcdn.net/images/artist/f062b593c5913988424eb53344da5f0d/1000x1000-000000-80-0-0.jpg',
		tracklist: 'https://api.deezer.com/artist/7099473/top?limit=50',
		type: 'artist'
	},
	album: {
		id: '41715861',
		title: 'More Than You Know',
		cover: 'https://api.deezer.com/album/41715861/image',
		cover_small:
			'https://e-cdns-images.dzcdn.net/images/cover/f85129789df4495478e970a47b8f957f/56x56-000000-80-0-0.jpg',
		cover_medium:
			'https://e-cdns-images.dzcdn.net/images/cover/f85129789df4495478e970a47b8f957f/250x250-000000-80-0-0.jpg',
		cover_big:
			'https://e-cdns-images.dzcdn.net/images/cover/f85129789df4495478e970a47b8f957f/500x500-000000-80-0-0.jpg',
		cover_xl:
			'https://e-cdns-images.dzcdn.net/images/cover/f85129789df4495478e970a47b8f957f/1000x1000-000000-80-0-0.jpg',
		tracklist: 'https://api.deezer.com/album/41715861/tracks',
		type: 'album'
	},
	type: 'track'
};

export default songBaseDetails;
