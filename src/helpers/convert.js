export function convertData(data) {
	return data.reduce((prev, next) => {
		prev[next.id] = next;
		return prev;
	}, {});
}

export function convertTitle(title, num = 32) {
	return title.length > num ? `${title.slice(0, num)}...` : title;
}
