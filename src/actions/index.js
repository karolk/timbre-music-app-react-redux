const PROXY = 'https://cors-anywhere.herokuapp.com/';
const URL = 'https://api.deezer.com/chart/0/';
const BASE_URL = 'https://api.deezer.com';
const SEARCH_URL = 'https://api.deezer.com/search/track?q=';

const limit = '?limit=';
const song_number = 16;

export const FETCH_ALBUMS = 'fetch_albums';
export const FETCH_CHART = 'fetch_chart';

export const FETCH_ALBUM_TRACKS = 'fetch_album_tracks';
export const FETCH_CHART_TRACKS = 'fetch_chart_tracks';
export const FETCH_SEARCH_TRACKS = 'fetch_search_tracks';

export const SET_LIBRARY_TRACKS = 'set_library_tracks';

export const SELECTED_ALBUM = 'selected_album';
export const SELECTED_SONG = 'current_song';

export const ADD_TO_LIBRARY = 'add_to_library';
export const REMOVE_FROM_LIBRARY = 'remove_from_favorite';

export const IS_FATCHED = 'is_fatched';
export const PLAY = 'play';

export const DURATION = 'duration';
export const CURRENT_TIME = 'current_time';

export const VOLUME_RANGE = 'volume_range';

export const OPEN_MOBILE_MENU = 'open_mobile_menu';

export const CLEAR = 'clear';
export const CLEAR_SELECTED_ALBUM = 'clear_selected_album';

const status = (response) => {
	if (response.status >= 200 && response.status < 400) {
		return Promise.resolve(response);
	} else {
		Promise.reject({
			status: response.status,
			statusText: response.statusText
		});
	}
};

const Header = new Headers({
	'Access-Control-Allow-Origin': 'http://localhost:3000/',
	'Content-Type': 'application/json',
	'Access-Control-Request-Method': 'GET',
	'Access-Control-Allow-Credentials': 'true',
	mode: 'no-cors'
});

const fetchData = (url) => {
	return fetch(url, Header)
		.then(status)
		.then((data) => data.json())
		.catch((error) => alert(Error(`Request filed HTTP status`)));
};

export function fetchAlbums(path) {
	const url = `${PROXY}${URL}${path}${limit}${song_number}`;

	const request = fetchData(url);

	return {
		type: FETCH_ALBUMS,
		payload: request
	};
}

export async function fetchAlbumTracks(tracks_url, callback) {
	const url = `${PROXY}${tracks_url}${limit}`;
	const request = await fetchData(url);
	await callback();

	return {
		type: FETCH_ALBUM_TRACKS,
		payload: request
	};
}

export async function fetchChartTracks(tracks_url, callback, song_number = 50) {
	const url = `${PROXY}${BASE_URL}${tracks_url}tracks${limit}${song_number}`;
	const request = await fetchData(url);
	await callback();

	return {
		type: FETCH_CHART_TRACKS,
		payload: request
	};
}

export async function fetchSearchTracks(query, callback) {
	query = query.trim().replace(/\s/g, '%20');
	const url = `${PROXY}${SEARCH_URL}${query}`;
	const request = await fetchData(url);
	await callback();

	return {
		type: FETCH_SEARCH_TRACKS,
		payload: request
	};
}

export function setLibraryTracks(library) {
	return {
		type: SET_LIBRARY_TRACKS,
		payload: library
	};
}

export function fetchState(state = false) {
	return {
		type: IS_FATCHED,
		payload: state
	};
}

export function selectedAlbum(album, callback) {
	callback();
	return {
		type: SELECTED_ALBUM,
		payload: album
	};
}

export function fetchChart(path_url) {
	const url = `${PROXY}${BASE_URL}${path_url}`;
	const request = fetchData(url);

	return {
		type: FETCH_CHART,
		payload: request
	};
}

export function addToLibrary(song) {
	return {
		type: ADD_TO_LIBRARY,
		payload: song
	};
}

export function removeFromLibrary(status) {
	return {
		type: REMOVE_FROM_LIBRARY,
		payload: status
	};
}

export function changeIndex(index = 0) {
	return {
		type: SELECTED_SONG,
		payload: index
	};
}

export function playAudio(state = false) {
	return {
		type: PLAY,
		payload: state
	};
}

export function audioDuration(duration) {
	return {
		type: DURATION,
		payload: duration
	};
}

export function audioCurrentTime(time) {
	return {
		type: CURRENT_TIME,
		payload: time
	};
}

export function volumeRange(range) {
	return {
		type: VOLUME_RANGE,
		payload: range
	};
}

export function openMobileMenu(status) {
	return {
		type: OPEN_MOBILE_MENU,
		payload: status
	};
}

export function clear() {
	return {
		type: CLEAR,
		payload: {}
	};
}

export function clearAlbum() {
	return {
		type: CLEAR_SELECTED_ALBUM,
		payload: {}
	};
}
