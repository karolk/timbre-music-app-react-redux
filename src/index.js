import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
// Import Redux store
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import promise from 'redux-promise';
import { composeWithDevTools } from 'redux-devtools-extension';

// Import reducers
import reducer from './reducers';

// Import css
import './css/main.css';

// Import components
import Header from './containers/Header';
import Sidebar from './containers/Sidebar';
import Activity from './components/Activity';
import App from './components/App';
import Player from './containers/Player';

// Create store
const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

const Root = () => (
	<BrowserRouter>
		<Provider store={createStoreWithMiddleware(reducer, composeWithDevTools())}>
			<div className="container">
				<Header />
				<div className="content">
					<Sidebar />
					<App />
					<Activity />
				</div>
				<Player />
			</div>
		</Provider>
	</BrowserRouter>
);

ReactDOM.render(<Root />, document.getElementById('root'));
