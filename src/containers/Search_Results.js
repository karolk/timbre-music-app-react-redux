import React from 'react';
import Song from '../containers/Song';
import Loading from '../components/Loading';
import backdrop from '../images/backdrops/backdrop-ortII.png';

// Connect to redux
import { connect } from 'react-redux';
import { fetchSearchTracks, changeIndex, fetchState } from '../actions/index';

class SearchResults extends React.Component {
	componentDidMount() {
		const query = this.props.match.params.query;
		this.getSearchTracks(query);
	}

	componentDidUpdate(prevProps) {
		const query = this.props.match.params.query;
		if (prevProps.match.params.query !== query) {
			this.getSearchTracks(query);
		}
	}

	getSearchTracks = async (query) => {
		// Fetch the data
		await this.props.fetchState();
		await this.props.fetchSearchTracks(query, () =>
			this.props.fetchState(!this.props.isFatched)
		);
		await this.props.changeIndex();
	};

	checkTheSongs = () => {
		const query = this.props.match.params.query;
		// Chekc if searchTrack is not empty
		return Object.keys(this.props.tracks).length === 0 ? (
			<div className="results__message">
				<h2 className="heading-second">No results found for {`"${query}"`}</h2>
				<p className="results__message_description">
					Please make sure your words are spelled correctly or use less or
					different keywords.
				</p>
			</div>
		) : (
			<div className="results__bottom">
				<h3 className="heading-third">Top results for {`"${query}"`}</h3>
				<div className="results__titles">
					<span className="results__num">#</span>
					<p className="results__title">Title</p>
					<p className="results__artist">Artist</p>
					<p className="results__album">Album</p>
					<p className="results__plays">Dayliy Plays</p>
				</div>
				<Song />
			</div>
		);
	};

	render() {
		if (!this.props.isFatched) return <Loading />;
		return (
			<section className="section results">
				<img src={backdrop} alt="Backdrop" className="results__backdrop" />
				{this.checkTheSongs()}
			</section>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		tracks: state.albumTracks,
		isFatched: state.isFatched
	};
};

export default connect(mapstateToProps, {
	fetchSearchTracks,
	changeIndex,
	fetchState
})(SearchResults);
