import React from 'react';
import magnifyingGlass from '../images/icons/sprite-icons.svg';
import PropTypes from 'prop-types';

class Search extends React.Component {
	static contextTypes = {
		router: PropTypes.object
	};

	getUserQuery = (e) => {
		e.preventDefault();
		// Get The query
		const query = this.refs.query.value;
		// Reset the form
		e.currentTarget.reset();
		// Pushc to search results
		this.context.router.history.push(`/search/${query}`);
	};

	render() {
		return (
			<form onSubmit={this.getUserQuery} className="search-form">
				<button type="submit" className="btn btn--submit">
					<svg className="icon icon--search">
						<use xlinkHref={`${magnifyingGlass}#icon-search`} />
					</svg>
				</button>
				<input
					ref="query"
					type="text"
					className="search-form__input-text"
					placeholder="Search"
					required
				/>
			</form>
		);
	}
}

export default Search;
