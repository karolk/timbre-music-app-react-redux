import React from 'react';
import Loading from '../components/Loading';

// Import component
import Song from './Song';

// Connect to redux
import { connect } from 'react-redux';
import {
	fetchChartTracks,
	fetchChart,
	changeIndex,
	fetchState
} from '../actions/index';

class Chart extends React.Component {
	componentDidMount() {
		this.setUpPath();
	}

	componentDidUpdate(prevProps) {
		let pathname = this.props.location.pathname;
		if (prevProps.location.pathname !== pathname) {
			this.setUpPath();
		}
	}

	setUpPath = () => {
		// Change the fetch state
		this.props.fetchState();
		// Get the playlist url path
		const playlist_url = this.props.location.pathname;
		// Get the tracks url path
		const tracks_url = `${playlist_url}`;
		// Get details
		this.getPlaylistDetails(tracks_url, playlist_url);
	};

	getPlaylistDetails = async (tracks_url, playlist_url) => {
		// Fetch the data
		await this.props.fetchChart(playlist_url);
		await this.props.fetchChartTracks(tracks_url, () =>
			this.props.fetchState(!this.props.isFatched)
		);
		await this.props.changeIndex();
	};

	converTime = (time) => {
		let [hours, min] = (time / 3600)
			.toFixed(2)
			.toString()
			.split('.');
		hours = parseInt(hours);
		min = parseInt(min);
		if (min > 60) {
			hours += 1;
			min -= 60;
		}
		return `${hours} hr ${min} min`;
	};

	render() {
		const playlist = this.props.selectedAlbum.album;
		const path_id = this.props.match.params.id;
		// Chekc if tracks are fetched or playlist is udefined
		if (
			!this.props.isFatched ||
			playlist === undefined ||
			(path_id === 'album' && playlist.upc === undefined) ||
			(path_id === 'playlist' && playlist.creator === undefined)
		)
			return <Loading />;

		return (
			<section className="section chart">
				<div className="chart__top">
					<img
						src={playlist.picture_medium || playlist.cover_medium}
						alt="Chart Cover"
						className="chart__cover"
					/>
					<div className="chart__description">
						<p className="chart__type">{playlist.type}</p>
						<h2 className="heading-second">{playlist.title}</h2>
						<p className="chart__text">
							Your daily update of the most played tracks right now
						</p>
						<p className="chart__creator">
							Created by:{' '}
							{playlist.creator !== undefined
								? playlist.creator.name
								: playlist.artist.name}
						</p>
						<p className="chart__followers">
							Followers:
							<span className="chart__nb_followers">
								{playlist.fans.toLocaleString()}
							</span>
						</p>
						<div className="chart__details">
							<p className="chart__nb_tracks">
								{Object.keys(this.props.tracks).length} songs
							</p>
							<p className="chart__nb_tracks">
								{this.converTime(playlist.duration)}
							</p>
						</div>
					</div>
				</div>
				<div className="chart__bottom">
					<div className="chart__titles">
						<span className="chart__num">#</span>
						<p className="chart__title">Title</p>
						<p className="chart__artist">Artist</p>
						<p className="chart__album">Album</p>
						<p className="chart__plays">Dayliy Plays</p>
					</div>
					<Song />
				</div>
			</section>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		isFatched: state.isFatched,
		tracks: state.albumTracks,
		selectedAlbum: state.selectedAlbum
	};
};

export default connect(mapstateToProps, {
	fetchChartTracks,
	fetchChart,
	changeIndex,
	fetchState
})(Chart);
