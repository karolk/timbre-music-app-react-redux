import React from 'react';
import icon from '../images/icons/sprite-icons.svg';
import user from '../images/users/user-5.png';
import Search from './Search';

// Connect to redux
import { connect } from 'react-redux';
import { openMobileMenu } from '../actions/index';

class Header extends React.Component {
	openMenu = () => this.props.openMobileMenu(!this.props.isMobileMenuOpend);

	render() {
		return (
			<header className="header">
				<div className="header__logo">
					<h3 className="header__logo-text">Timbre</h3>
					<svg className="icon icon--logo">
						<use xlinkHref={`${icon}#icon-graphic_eq`} />
					</svg>
				</div>
				<button onClick={this.openMenu} className="btn btn--mobile_menu">
					<svg
						className={`icon icon--logo icon--menu_mobile
						${this.props.isMobileMenuOpend ? `icon--opend` : ''}`}
					>
						<use xlinkHref={`${icon}#icon-graphic_eq`} />
					</svg>
				</button>
				<Search />
				<div className="header__user">
					<img src={user} alt="User" className="header__user-img" />
					<p className="header__user-name">Michale Kors</p>
				</div>
				<button className="btn btn--log-out">
					<svg className="icon icon--log-out">
						<use xlinkHref={`${icon}#icon-navigate_next`} />
					</svg>
				</button>
			</header>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		isMobileMenuOpend: state.isMobileMenuOpend
	};
};

export default connect(mapstateToProps, { openMobileMenu })(Header);
