import React from 'react';
import backdrop from '../images/backdrops/backdrop-side-left.png';
import Nav from '../containers/Nav';
import SongDetails from './Song_Details';

// Connect to redux
import { connect } from 'react-redux';

class Sidebar extends React.Component {
	openMenu = () => (this.props.isMobileMenuOpend ? `sidebar--mobile_open` : '');

	render() {
		return (
			<div className={`sidebar ${this.openMenu()}`}>
				<img src={backdrop} alt="Backdrop" className="sidebar__backdrop" />
				<h4 className="heading-fourth">Your Music</h4>
				<Nav />
				<SongDetails />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		isMobileMenuOpend: state.isMobileMenuOpend
	};
};

export default connect(mapStateToProps)(Sidebar);
