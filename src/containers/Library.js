import React from 'react';
import backdrop from '../images/backdrops/backdrop-library-small.png';
import Song from '../containers/Song';

// Connect to redux
import { connect } from 'react-redux';
import {
	addToLibrary,
	removeFromLibrary,
	changeIndex,
	setLibraryTracks
} from '../actions/index';

class Library extends React.Component {
	componentDidMount() {
		// Get Local storage
		const storage = JSON.parse(localStorage.getItem('library'));
		const library = storage || this.props.library;
		// Add song to library
		this.props.addToLibrary(library);
		// Chek if library is empty
		if (Object.keys(library).length !== 0) {
			// Set album tracks
			this.props.setLibraryTracks(library);
			// Change index
			this.props.changeIndex();
		}
	}

	componentDidUpdate(prevProps) {
		const library = this.props.library;
		// Get Local storage
		const library_storage = JSON.stringify(this.props.library);
		if (Object.keys(prevProps.library).length !== Object.keys(library).length) {
			// Set album tracks
			this.props.setLibraryTracks(library);
			// Update Local Storage
			localStorage.setItem('library', library_storage);
		}
	}

	checkTheSongs = () => {
		// Check if library is not empty
		return Object.keys(this.props.library).length === 0 ? (
			<div className="library__message">
				<h2 className="heading-second">Your library is empty</h2>
				<p className="library__message_description">
					Songs will be available when you add them to your library.
				</p>
			</div>
		) : (
			<div className="library__bottom">
				<h3 className="heading-third">{`Your library includes ${
					Object.keys(this.props.library).length
				} songs`}</h3>
				<div className="library__titles">
					<span className="library__num">#</span>
					<p className="library__title">Title</p>
					<p className="library__artist">Artist</p>
					<p className="library__album">Album</p>
					<p className="library__plays">Dayliy Plays</p>
				</div>
				<Song />
			</div>
		);
	};

	render() {
		return (
			<section className="section library">
				<img src={backdrop} alt="Backdrop" className="library__backdrop" />
				{this.checkTheSongs()}
			</section>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		tracks: state.albumTracks,
		isFatched: state.isFatched,
		library: state.library
	};
};

export default connect(mapstateToProps, {
	addToLibrary,
	removeFromLibrary,
	changeIndex,
	setLibraryTracks
})(Library);
