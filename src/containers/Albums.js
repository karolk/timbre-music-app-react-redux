import React from 'react';
import backdrop from '../images/backdrops/backdrop-chet-faker-small.png';

// Connect to redux
import { connect } from 'react-redux';
import { fetchAlbums, clear } from '../actions/index';

// Import components
import Album from './Album';
import Artist from './Artist';
import NavAlbums from '../components/Nav_Albums';
import Loading from '../components/Loading';

class Albums extends React.Component {
	componentDidMount() {
		//  Clear albums
		this.props.clear();
		// Fetch Albums
		this.props.fetchAlbums(this.props.match.params.id);
	}
	componentDidUpdate(prevProps, prevState) {
		//Check if new path is diffrent, than fetch the albums
		if (prevProps.match.params.id !== this.props.match.params.id) {
			//  Clear albums
			this.props.clear();
			// Fetch New Albums
			this.props.fetchAlbums(this.props.match.params.id);
		}
	}

	check = () => {
		if (Object.keys(this.props.albums).length === 0) return <Loading />;
	};

	render() {
		const path = this.props.match.params.id;
		// Remove last letter from the path playlists -> playlist
		const path_url = path.slice(0, path.length - 1);
		return (
			<section className="section albums">
				<img src={backdrop} alt="Backdrop" className="albums__backdrop" />
				<NavAlbums />
				{this.check()}
				{path !== 'artists' ? <Album url={path_url} /> : <Artist />}
			</section>
		);
	}
}

const mapstateToProps = (state) => {
	return { albums: state.albums };
};

export default connect(mapstateToProps, { fetchAlbums, clear })(Albums);
