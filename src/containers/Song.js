import React from 'react';
import icon from '../images/icons/sprite-icons.svg';
import { convertTitle } from '../helpers/convert';

// Connect to redux
import { connect } from 'react-redux';
import { changeIndex } from '../actions/index';

class Song extends React.Component {
	playSong = (id) => {
		// Get the tracks
		const tracks = Object.keys(this.props.tracks);
		// find song index
		const index = tracks.findIndex((index) => parseInt(index) === id);
		// Change index
		this.props.changeIndex(index);
	};

	checkSongAlbum = (album) =>
		//Check if track has an album's title
		album !== undefined
			? convertTitle(album.title, 12)
			: convertTitle(this.props.selectedAlbum.album.title, 12);

	render() {
		const tracks = this.props.tracks;
		return (
			<ul className="chart__list">
				{Object.values(tracks).map((song, index) => {
					return (
						<li key={song.id} className="song">
							<div className="song__controller">
								<span className="song__number">{index + 1}</span>
								<button
									onClick={() => this.playSong(song.id)}
									className="btn btn--play btn--song_play song__play"
								>
									<svg className="icon icon--controller icon--play_song">
										<use xlinkHref={`${icon}#icon-play_arrow`} />
									</svg>
								</button>
							</div>
							<p className="song__title">{convertTitle(song.title)}</p>
							<p className="song__artist">
								{convertTitle(song.artist.name, 12)}
							</p>
							<p className="song__album">{this.checkSongAlbum(song.album)}</p>
							<span className="song__plays">{song.rank.toLocaleString()}</span>
						</li>
					);
				})}
			</ul>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		tracks: state.albumTracks,
		selectedAlbum: state.selectedAlbum
	};
};

export default connect(mapstateToProps, { changeIndex })(Song);
