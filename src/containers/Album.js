import React from 'react';
import icon from '../images/icons/sprite-icons.svg';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

// Connect to redux
import { connect } from 'react-redux';
import {
	fetchAlbumTracks,
	selectedAlbum,
	changeIndex,
	fetchState,
	clearAlbum
} from '../actions/index';

class Album extends React.Component {
	setAlbum = (id) => {
		const album = this.props.albums[id];
		const tracks_url = album.tracklist;
		return { album, tracks_url };
	};

	selectedAlbum = async (id) => {
		// Get the album and tracklist
		const { album, tracks_url } = this.setAlbum(id);
		// Fetch the data
		this.props.clearAlbum();
		await this.props.selectedAlbum(album, async () => {
			await this.props.fetchState();
			await this.props.fetchAlbumTracks(tracks_url, () =>
				this.props.fetchState(!this.props.isFatched)
			);
			await this.props.changeIndex();
		});
	};

	render() {
		const albums = this.props.albums;
		return (
			<ul className="albums__content">
				{Object.values(albums).map((album) => (
					<li key={album.id} className="album">
						<figure className="album__cover">
							<img
								src={album.cover_medium || album.picture_medium}
								alt="Album Cover"
								className="album__cover-img"
							/>
							<div className="album__player">
								{this.props.isFatched ? (
									<button
										onClick={() => {
											this.selectedAlbum(album.id);
										}}
										className="btn btn--play btn--album_play album__play"
									>
										<svg className="icon icon--controller icon--controller_album">
											<use xlinkHref={`${icon}#icon-graphic_eq`} />
										</svg>
									</button>
								) : (
									<div className="loading loading--small">
										<span className="album__loading loading__spinner loading__spinner--small">
											&nbsp;
										</span>
									</div>
								)}
							</div>
						</figure>
						<Link
							to={`/${this.props.url}/${album.id}/`}
							className="album__link"
						>
							{album.title}
						</Link>
						{album.artist ? (
							<p className="album__artist">{album.artist.name}</p>
						) : (
							''
						)}
					</li>
				))}
			</ul>
		);
	}

	static propTypes = {
		url: PropTypes.string
	};
}

const mapstateToProps = (state) => {
	return {
		albums: state.albums,
		isFatched: state.isFatched
	};
};

export default connect(mapstateToProps, {
	fetchAlbumTracks,
	selectedAlbum,
	changeIndex,
	fetchState,
	clearAlbum
})(Album);
