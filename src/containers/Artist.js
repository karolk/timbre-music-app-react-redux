import React from 'react';
import icon from '../images/icons/sprite-icons.svg';

// Connect to redux
import { connect } from 'react-redux';
import {
	fetchAlbumTracks,
	selectedAlbum,
	playAudio,
	changeIndex,
	fetchState
} from '../actions/index';

class Artist extends React.Component {
	setAlbum = (id) => {
		const album = this.props.albums[id];
		const tracks_url = album.tracklist;
		return { album, tracks_url };
	};

	selectedAlbum = async (id) => {
		// Get the album and tracklist
		const { album, tracks_url } = this.setAlbum(id);

		// Fetch the data
		await this.props.fetchState();
		await this.props.fetchAlbumTracks(tracks_url, () =>
			this.props.fetchState(!this.props.isFatched)
		);
		await this.props.changeIndex();
		await this.props.selectedAlbum(album);
	};

	render() {
		const artists = this.props.albums;
		return (
			<ul className="artists__content">
				{Object.values(artists).map((artist) => {
					return (
						<li key={artist.id} className="artist">
							<figure className="artist__cover">
								<img
									src={artist.picture_medium}
									alt="artist Cover"
									className="artist__cover-img"
								/>
								<div className="artist__player">
									{this.props.isFatched ? (
										<button
											onClick={() => {
												this.selectedAlbum(artist.id);
											}}
											className="btn btn--play btn--album_play artist__play"
										>
											<svg className="icon icon--controller icon--controller_album">
												<use xlinkHref={`${icon}#icon-graphic_eq`} />
											</svg>
										</button>
									) : (
										<div className="loading loading--small">
											<span className="album__loading loading__spinner loading__spinner--small">
												&nbsp;
											</span>
										</div>
									)}
								</div>
							</figure>
							<p className="artist__name">{artist.name}</p>
						</li>
					);
				})}
			</ul>
		);
	}
}
const mapstateToProps = (state) => {
	return { albums: state.albums, isFatched: state.isFatched };
};

export default connect(mapstateToProps, {
	fetchAlbumTracks,
	selectedAlbum,
	playAudio,
	changeIndex,
	fetchState
})(Artist);
