import React from 'react';
import icon from '../images/icons/sprite-icons.svg';
import songBaseDetails from '../helpers/song_details';

// Connect to redux
import { connect } from 'react-redux';
import {
	playAudio,
	changeIndex,
	audioDuration,
	audioCurrentTime,
	volumeRange
} from '../actions/index';

class Player extends React.Component {
	skip = 1;

	componentDidMount() {
		// Load the song
		this.audio.onloadeddata = () =>
			this.props.audioDuration(this.audio.duration);

		// Update the current time of the song
		this.audio.ontimeupdate = () =>
			this.props.audioCurrentTime(this.audio.currentTime);
	}

	componentDidUpdate() {
		// Update the range volume
		this.audio.volume = this.props.volume;
		// Update the progress bar
		this.handleProgressBar();
		// Update the volume range filled
		this.handeRangeFiiled();
		// Player controller
		this.playController();
	}

	setSong = () => {
		let preview = songBaseDetails.preview;
		const tracks = this.props.tracks;
		// Check If there are tracks
		if (Object.keys(tracks).length !== 0) {
			//Get current index
			const index = this.props.songIndex;
			// Get Song id
			const id = Object.keys(tracks)[index];
			// Get song
			const song = tracks[id];
			return song !== undefined ? song.preview || preview : preview;
		}
		return preview;
	};

	playController = () => {
		// Get the isPlayed state
		const state = this.props.isPlayed;
		// If state is true play
		const method = state ? 'play' : 'pause';
		this.audio[method]();
	};

	playAudio = () => this.props.playAudio(!this.props.isPlayed);

	getCurrentState = () => {
		return {
			tracks: Object.keys(this.props.tracks),
			index: this.props.songIndex
		};
	};

	playNext = () => {
		// Get track and current index
		let { tracks, index } = this.getCurrentState();
		if (tracks.length > 0) {
			// Skip to next index
			index += this.skip;
			// If index is grater than albums length, change index to 0
			index > tracks.length - 1
				? this.props.changeIndex()
				: this.props.changeIndex(index);
		} else {
			this.props.changeIndex();
		}
	};

	playPrev = () => {
		// Get track and current index
		let { tracks, index } = this.getCurrentState();
		if (tracks.length > 0) {
			// Skip to previous index
			index -= this.skip;

			// If index is equal to -1, change index to the last song in the album
			index === -1
				? this.props.changeIndex(tracks.length - 1)
				: this.props.changeIndex(index);
		} else {
			this.props.changeIndex();
		}
	};

	convertTime = (time) => {
		if (time !== null) {
			let [sec, mili] = time.toString().split('.');
			sec = parseInt(sec, 0);
			mili = parseInt(mili, 0);
			if (mili > 60) {
				sec += this.skip;
				mili = 0;
			}
			if (sec < 10) sec = `0${sec}`;
			return `0:${sec}`;
		}

		return '0:00';
	};

	handleProgressBar = () => {
		// Count the precentage. Current time divided by song duration
		const percent = this.props.songCurrentTime / this.props.songDuration * 100;
		this.progressBar.style.flexBasis = `${percent}%`;
	};

	handeRangeFiiled = () => {
		const volume = parseFloat(this.props.volume);
		this.rangeFilled.style.width = `${volume * 100}%`;
	};

	changeCurrentTime = (e) => {
		// Get the width of the bar
		let width = window.getComputedStyle(this.refs.bar).width.replace(/px/g, '');
		width = parseInt(width, 0);
		// See the options
		// e.persist();
		// See Mouse Event options
		// console.log(e.nativeEvent);

		// Count the new current time
		const changeTime = e.nativeEvent.offsetX / width * this.props.songDuration;

		this.audio.currentTime = changeTime;
	};

	changeVolumeRange = () => this.props.volumeRange(this.range.value);

	render() {
		return (
			<div className="audio-player">
				<div className="player">
					<div className="player__controllers">
						<button onClick={this.playPrev} className="btn btn--prev">
							<svg className="icon icon--controller">
								<use xlinkHref={`${icon}#icon-skip_previous`} />
							</svg>
						</button>
						<button onClick={this.playAudio} className="btn btn--play">
							<svg className="icon icon--controller">
								<use
									xlinkHref={`${icon}#${
										this.props.isPlayed ? 'icon-pause' : 'icon-play_arrow'
									}`}
								/>
							</svg>
						</button>
						<button onClick={this.playNext} className="btn btn--next">
							<svg className="icon icon--controller">
								<use xlinkHref={`${icon}#icon-skip_next`} />
							</svg>
						</button>
					</div>
					<div className="player__progress">
						<span className="player__current-time">
							{this.convertTime(this.props.songCurrentTime)}
						</span>
						<div
							className="player__bar"
							ref="bar"
							onClick={this.changeCurrentTime}
						>
							<span
								ref={(progressBar) => (this.progressBar = progressBar)}
								className="player__filld"
							>
								&nbsp;
							</span>
							<span className="player__bar-thumb">&nbsp;</span>
						</div>
						<span className="player__time">
							{this.convertTime(this.props.songDuration)}
						</span>
					</div>
					<div className="player__volume">
						<svg className="icon icon--volume">
							<use xlinkHref={`${icon}#icon-volume_up`} />
						</svg>
						<div className="player__slider">
							<input
								ref={(range) => (this.range = range)}
								onChange={this.changeVolumeRange}
								className="player__range"
								type="range"
								name="range"
								min="0"
								max="1"
								defaultValue="0.8"
								step="0.01"
							/>
							<span
								ref={(rangeFilled) => (this.rangeFilled = rangeFilled)}
								className="player__range-filled"
							>
								&nbsp;
							</span>
						</div>
					</div>
				</div>
				<audio
					ref={(audio) => {
						this.audio = audio;
					}}
					className="audio"
					src={this.setSong()}
					type="audio/wav"
				>
					Your browser does not support the <code>audio</code> element.
				</audio>
			</div>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		isPlayed: state.isPlayed,
		selectedAlbum: state.selectedAlbum,
		tracks: state.albumTracks,
		isFatched: state.isFatched,
		songIndex: state.songIndex,
		songDuration: state.songDuration,
		songCurrentTime: state.songCurrentTime,
		volume: state.volume
	};
};

export default connect(mapstateToProps, {
	playAudio,
	changeIndex,
	audioDuration,
	audioCurrentTime,
	volumeRange
})(Player);
