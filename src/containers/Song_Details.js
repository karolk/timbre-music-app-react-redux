import React from 'react';
import Loading from '../components/Loading';
import { convertTitle } from '../helpers/convert';
import icon from '../images/icons/sprite-icons.svg';

// Base details
import songBaseDetails from '../helpers/song_details';

// Connect to redux
import { connect } from 'react-redux';
import { addToLibrary, removeFromLibrary } from '../actions/index';

class SongDetails extends React.Component {
	componentDidMount() {
		// Get Local storage
		const storage = JSON.parse(localStorage.getItem('library'));
		// Chek if library is empty
		const library = storage || this.props.library;
		// Add song to library
		this.props.addToLibrary(library);
	}

	componentDidUpdate(prevProps) {
		const library = this.props.library;
		// Get Local storage
		const library_storage = JSON.stringify(this.props.library);
		if (Object.keys(prevProps.library).length !== Object.keys(library).length) {
			// Add song to library
			this.props.addToLibrary(library);
			// Update Local Storage
			localStorage.setItem('library', library_storage);
		}
	}

	checkCover = (album) => {
		// Check if album has the cover
		return album === undefined
			? this.props.selectedAlbum.album.cover_medium ||
					songBaseDetails.album.cover_medium
			: album.cover_medium;
	};

	addSong = (song) => {
		const library_songs = this.props.library;
		// Check if song has info about album, if not get the selectedAlbum info
		const cover = () => {
			if (song.album === undefined) {
				return {
					album: { ...this.props.selectedAlbum.album }
				};
			}
		};
		// Check if song is added to the library
		const findID = Object.keys(library_songs).find(
			(id) => parseInt(id) === song.id
		);
		// If is not in library, add it
		if (!findID) {
			const song_cover = cover();
			// Create the song details info
			const song_details = {
				[song.id]: {
					...song,
					...song_cover
				}
			};
			this.props.addToLibrary(song_details);
		}
	};

	checkTheSongInLibrary = (song) => {
		const library_songs = this.props.library;
		// Check if song is added to the library
		const findID = Object.keys(library_songs).find(
			(id) => parseInt(id) === song.id
		);
		// Check if song is in the library, return button remove,
		return findID ? (
			<button
				onClick={() => this.props.removeFromLibrary(song.id)}
				className="btn btn--remove"
			>
				<svg className="icon icon--controller">
					<use xlinkHref={`${icon}#icon-check`} />
				</svg>
			</button>
		) : (
			<button onClick={() => this.addSong(song)} className="btn btn--add">
				<svg className="icon icon--controller">
					<use xlinkHref={`${icon}#icon-add`} />
				</svg>
			</button>
		);
	};

	setUpSongDetails = () => {
		// Get current index of the song
		const index = this.props.songIndex;
		// Get the song id
		const id = Object.keys(this.props.tracks)[index];
		// Get song details
		let song = this.props.tracks[id];
		//I f there is not a song info return base info
		song = song === undefined ? songBaseDetails : song;

		return (
			<div className="song-details">
				<div className="song-details__cover_box">
					<img
						src={this.checkCover(song.album)}
						alt="Song Cover"
						className="song-details__cover"
					/>
					<div className="song-details__add">
						{this.checkTheSongInLibrary(song)}
					</div>
				</div>
				<p className="song-details__title">{convertTitle(song.title, 20)}</p>
				<p className="song-details__artist">{song.artist.name}</p>
			</div>
		);
	};

	render() {
		// If the track are fetching return loading
		if (!this.props.isFatched) return <Loading />;
		return this.setUpSongDetails();
	}
}

const mapstateToProps = (state) => {
	return {
		tracks: state.albumTracks,
		isFatched: state.isFatched,
		songIndex: state.songIndex,
		selectedAlbum: state.selectedAlbum,
		library: state.library
	};
};

export default connect(mapstateToProps, {
	addToLibrary,
	removeFromLibrary
})(SongDetails);
