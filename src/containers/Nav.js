import React from 'react';
import { NavLink } from 'react-router-dom';
import icon from '../images/icons/sprite-icons.svg';

// Connect to redux
import { connect } from 'react-redux';
import { openMobileMenu } from '../actions/index';

class Nav extends React.Component {
	openMenu = () => this.props.openMobileMenu(!this.props.isMobileMenuOpend);

	render() {
		return (
			<nav className="nav">
				<ul className="nav__list">
					<li className="nav__item">
						<NavLink
							onClick={this.openMenu}
							exact
							to="/music/playlists"
							className="nav__link"
						>
							<svg className="icon icon--nav">
								<use xlinkHref={`${icon}#icon-grain`} />
							</svg>
							Browse
						</NavLink>
					</li>
					<li className="nav__item">
						<NavLink
							onClick={this.openMenu}
							to="/playlist/3155776842/"
							className="nav__link"
						>
							<svg className="icon icon--nav">
								<use xlinkHref={`${icon}#icon-music_note`} />
							</svg>
							Songs
						</NavLink>
					</li>
					<li className="nav__item">
						<NavLink
							onClick={this.openMenu}
							to="/music/albums"
							className="nav__link"
						>
							<svg className="icon icon--nav">
								<use xlinkHref={`${icon}#icon-adjust`} />
							</svg>
							Albums
						</NavLink>
					</li>
					<li className="nav__item">
						<NavLink
							onClick={this.openMenu}
							to="/music/artists"
							className="nav__link"
						>
							<svg className="icon icon--nav">
								<use xlinkHref={`${icon}#icon-account_circle`} />
							</svg>
							Artists
						</NavLink>
					</li>
					<li className="nav__item">
						<NavLink
							onClick={this.openMenu}
							to="/playlist/your-library"
							className="nav__link"
						>
							<svg className="icon icon--nav">
								<use xlinkHref={`${icon}#icon-check`} />
							</svg>
							Your Library
						</NavLink>
					</li>
				</ul>
			</nav>
		);
	}
}

const mapstateToProps = (state) => {
	return {
		isMobileMenuOpend: state.isMobileMenuOpend
	};
};

export default connect(mapstateToProps, { openMobileMenu })(Nav);
