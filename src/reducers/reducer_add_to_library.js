import { ADD_TO_LIBRARY, REMOVE_FROM_LIBRARY } from '../actions/index';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case ADD_TO_LIBRARY:
			return { ...state, ...action.payload };

		case REMOVE_FROM_LIBRARY:
			const id_to_remove = action.payload;
			const newState = { ...state };
			delete newState[id_to_remove];
			return newState;

		default:
			return state;
	}
}
