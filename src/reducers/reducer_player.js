import { PLAY } from '../actions';

const INITIAL_STATE = false;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case PLAY:
			return action.payload;

		default:
			return state;
	}
}
