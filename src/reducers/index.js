import { combineReducers } from 'redux';
import AlbumsReducer from './reducer_albums';
import PLayReducer from './reducer_player';
import AlbumTracksReducer from './reducer_album_tracks';
import SelectedAlbumReducer from './reducer_album_selected';
import SongIndexReducer from './reducer_current_song';
import isFatchedReducer from './reducer_is_fatched';
import DurationReducer from './reducer_audio_duration';
import CurrentTimeReducer from './reducer_current_time';
import VolumeRangeReducer from './reducer_volume_range';
import AddToLibraryReducer from './reducer_add_to_library';
import OpenMobileMenuReducer from './reducer_open_mobile_menu';

const rootReducer = combineReducers({
	albums: AlbumsReducer,
	selectedAlbum: SelectedAlbumReducer,
	albumTracks: AlbumTracksReducer,
	library: AddToLibraryReducer,
	isPlayed: PLayReducer,
	isFatched: isFatchedReducer,
	songIndex: SongIndexReducer,
	songDuration: DurationReducer,
	songCurrentTime: CurrentTimeReducer,
	volume: VolumeRangeReducer,
	isMobileMenuOpend: OpenMobileMenuReducer
});

export default rootReducer;
