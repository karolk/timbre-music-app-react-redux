import { CURRENT_TIME } from '../actions/index';

const INITIAL_STATE = null;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case CURRENT_TIME:
			return action.payload;
		default:
			return state;
	}
}
