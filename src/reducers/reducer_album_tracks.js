import {
	FETCH_ALBUM_TRACKS,
	FETCH_CHART_TRACKS,
	FETCH_SEARCH_TRACKS,
	SET_LIBRARY_TRACKS
} from '../actions/index';
import { convertData } from '../helpers/convert';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case FETCH_ALBUM_TRACKS:
			return convertData(action.payload.data);

		case FETCH_CHART_TRACKS:
			return convertData(action.payload.data);

		case FETCH_SEARCH_TRACKS:
			return convertData(action.payload.data);

		case SET_LIBRARY_TRACKS:
			return action.payload;

		default:
			return state;
	}
}
