import { FETCH_ALBUMS, CLEAR } from '../actions/index';
import { convertData } from '../helpers/convert';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case FETCH_ALBUMS:
			return convertData(action.payload.data);

		case CLEAR:
			return action.payload;

		default:
			return state;
	}
}
