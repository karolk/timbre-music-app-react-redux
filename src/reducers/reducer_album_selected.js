import {
	SELECTED_ALBUM,
	FETCH_CHART,
	CLEAR_SELECTED_ALBUM
} from '../actions/index';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case SELECTED_ALBUM:
			return { album: action.payload };

		case FETCH_CHART:
			return { album: action.payload };

		case CLEAR_SELECTED_ALBUM:
			return action.payload;
		default:
			return state;
	}
}
