import { OPEN_MOBILE_MENU } from '../actions/index';

const INITIAL_STATE = false;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case OPEN_MOBILE_MENU:
			return action.payload;

		default:
			return state;
	}
}
