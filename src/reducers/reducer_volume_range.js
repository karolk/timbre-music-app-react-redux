import { VOLUME_RANGE } from '../actions/index';

const INITIAL_STATE = 0.8;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case VOLUME_RANGE:
			return action.payload;

		default:
			return state;
	}
}
