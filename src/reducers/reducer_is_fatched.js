import { IS_FATCHED } from '../actions/index';

const INITIAL_STATE = true;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case IS_FATCHED:
			return action.payload;

		default:
			return state;
	}
}
