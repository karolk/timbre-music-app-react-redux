import { DURATION } from '../actions/index';

const INITIAL_STATE = null;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case DURATION:
			return action.payload;
		default:
			return state;
	}
}
