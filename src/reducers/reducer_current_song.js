import { SELECTED_SONG } from '../actions/index';

const INITIAL_STATE = 0;

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case SELECTED_SONG:
			return action.payload;
		default:
			return state;
	}
}
