import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

// Import components
import Albums from '../containers/Albums';
import Chart from '../containers/Chart';
import SearchResults from '../containers/Search_Results';
import Library from '../containers/Library';
import NotFound from './Not_Found';

class App extends React.Component {
	render() {
		return (
			<main className="main">
				<Switch>
					<Redirect exact from="/" to="/music/playlists" />
					<Route path="/music/:id" component={Albums} />
					<Route path="/search/:query" component={SearchResults} />
					<Route exact path="/playlist/your-library" component={Library} />
					<Route path="/:id" component={Chart} />
					<Route component={NotFound} />
				</Switch>
			</main>
		);
	}
}

export default App;
