import React from 'react';
import { NavLink } from 'react-router-dom';

const NavAlbums = () => (
	<nav className="nav-albums">
		<ul className="nav-albums__list">
			<li className="nav-albums__item">
				<NavLink
					exact
					activeClassName="nav-albums__link--active"
					to="/music/playlists"
					className="nav-albums__link"
				>
					Browse
				</NavLink>
			</li>
			<li className="nav-albums__item">
				<NavLink
					activeClassName="nav-albums__link--active"
					to="/music/albums"
					className="nav-albums__link"
				>
					Albums
				</NavLink>
			</li>
			<li className="nav-albums__item">
				<NavLink
					activeClassName="nav-albums__link--active"
					to="/music/artists"
					className="nav-albums__link"
				>
					Artists
				</NavLink>
			</li>
			<li className="nav-albums__item">
				<NavLink
					activeClassName="nav-albums__link--active"
					to="/playlist/3155776842/"
					className="nav-albums__link"
				>
					Songs
				</NavLink>
			</li>
		</ul>
	</nav>
);

export default NavAlbums;
