import React from 'react';

const Loading = () => {
	return (
		<div className="load">
			<div className="loading">
				<span className="loading__spinner">&nbsp;</span>
			</div>
		</div>
	);
};

export default Loading;
