import React from 'react';
import backdrop from '../images/backdrops/backdrop-side-right.png';
import Users from '../helpers/users';
import icon from '../images/icons/sprite-icons.svg';

const Activity = () => {
	return (
		<div className="activity">
			<img src={backdrop} alt="Backdrop" className="activity__backdrop" />
			<ul className="activity__users">
				{Users.map(user => (
					<li key={user.name} className="activity__user">
						<div className="user">
							<img src={user.photo} alt="User" className="user__photo" />
							<div className="user__info">
								<p className="user__name">{user.name}</p>
								<p className="user__title">{user.title}</p>
								<p className="user__artist">{user.artist}</p>
							</div>
						</div>
						<svg className="icon icon--activity">
							<use xlinkHref={`${icon}#icon-grain`} />
						</svg>
					</li>
				))}
			</ul>
		</div>
	);
};

export default Activity;
