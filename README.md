# Timbre Music App

### Live [Timbre Music](https://timbre-music-app.netlify.com)

![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Ftimbre-playlist.png?alt=media&token=1b40511f-857f-4231-ac4c-bdeeb3d114cc)
![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Ftimre-top-songs.png?alt=media&token=c6a520cb-c6f8-4a5c-b789-3df51a8d7006)

### Description

With "Timbre", it’s easy to find the right music for every moment – on your phone, your computer, your tablet and more.

There are millions of tracks so whether you’re working out, partying or relaxing, the right music is always at your fingertips. Choose what you want to listen to.

You can browse through the music playlists, albums, artists, top songs, artists and create your on list / library with your favorites songs.

Inspired by Spotify and Apple Music.

### Design, project structure and implementation of the project have been implemented and designed by Karol Kozer.

### The project includes:

React, Redux, ES6, Sass, Deezer API

### Deezer API

Unlimited Access, without stress, without identification. Deezer Simple API provides a nice set of services to build up web applications allowing the discovery of Deezer's music catalogue.

[Deezer](https://developers.deezer.com/api)

### Run project

Require [Node](https://nodejs.org/en/)

Install the dependencies:

```
npm install
```

Run dev server:

```
npm run start
```

Localhost:
`http://localhost:3000/`
